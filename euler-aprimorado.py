import math
import matplotlib.pyplot as plt
from prettytable import PrettyTable

fixed = 4

# não modifique isso
xiPlusOne = lambda x, h: round(x + h, fixed)
yiPlusOne = lambda y, k1, k2: round(y + 0.5 * (k1 + k2), fixed)
k1Calc = lambda f, h: round(f * h, fixed)
k2Calc = lambda fk1, h: round(fk1 * h, fixed)

# não modifique as somas entre ()
fk1Calc = lambda x, h, y, k1: round(1 - (x + h) + 4 * (y + k1), fixed)

edo = lambda x , y: round(1 - x + 4 * y, fixed)

calcAnalytics = True
analyticsFunction = lambda x, y: round(1/4 * x - 3/16 + 19/16 * math.exp(4 * x), fixed)


def euler(x0, stop, h, y0):
  '''
  Parameters
  ----------
  x0 : int
    limite inferior do intervalo
  stop : int
    valor de xi+1, criterio de parada
  h : int
    tamanho do passo
  y0: int
    solucao inicial     
  '''
  x = [x0]
  y = [y0]

  # muda
  print(f'1 - {x[len(x) -1]} + 4 * {y[len(y) -1]} f\n')
  f = [edo(x[0], y[0])]

  print(f'{f[len(f) -1]} * {h} k1\n')
  k1 = [k1Calc(f[0], h)]

  # muda
  print(f'1 - ({x[len(x) -1]} + {h}) + 4 * ({y[len(y) -1]} + {k1[len(k1) -1]}) fk1\n')
  fk1 = [fk1Calc(x[0], h, y[0], k1[0])]

  print(f'{fk1[len(fk1) -1]} * {h}')
  k2 = [k2Calc(fk1[len(fk1) -1], h)]

  analytics = []
  error = []
  if (calcAnalytics):
    # muda
    print(f'1/4 * {x[len(x) -1]} - 3/16 + 19/16 * {math.exp(4 * x[len(x) -1])} Analytics\n')
    analytics.append(analyticsFunction(x[0], y[0]))

    print(f'|{analytics[len(analytics) -1]} - {y[len(y) -1]}| erro \n')
    error.append(abs(analytics[0] - y[0]))
   
  while (x[len(x) -1] < stop):
    print(f'{y[len(y) -1]} + 0.5 * ({k1[len(k1) -1]} + {k2[len(k2) -1]})')
    y.append(yiPlusOne(y[len(y) -1], k1[len(k1) -1], k2[len(k2) -1]))
    
    print(f'{x[len(x) -1]} + {h} x\n')
    x.append(xiPlusOne(x[len(x) -1], h))

    # muda
    print(f'1 - {x[len(x) -1]} + 4 * {y[len(y) -1]} f\n')
    f.append(edo(x[len(x) -1], y[len(y) -1]))

    print(f'{f[len(f) -1]} * {h} k1\n')
    k1.append(k1Calc(f[len(f) -1], h))
    
    # muda
    print(f'1 - ({x[len(x) -1]} + {h}) + 4 * ({y[len(y) -1]} + {k1[len(k1) -1]}) fk1\n')
    fk1.append(fk1Calc(x[len(x) -1], h, y[len(y) -1], k1[len(k1) -1]))

    print(f'{fk1[len(fk1) -1]} * {h}')
    k2.append(k2Calc(fk1[len(fk1) -1], h))
    
    if (calcAnalytics):
      # muda
      print(f'1/4 * {x[len(x) -1]} - 3/16 + 19/16 * {math.exp(4 * x[len(x) -1])} Analytics\n')
      analytics.append(analyticsFunction(x[len(x) -1], y[len(y) -1]))

      print(f'|{analytics[len(analytics) -1]} - {y[len(y) -1]}| erro \n')
      error.append(round(abs(analytics[len(analytics) -1] - y[len(y) -1]), fixed))
  
  table = PrettyTable()
  table.add_column('xi', x)
  table.add_column('yi', y)
  table.add_column("f'(xi, yi)", f)
  table.add_column('k1', k1)
  table.add_column("f'(xi+h, yi+k1)", fk1)
  table.add_column('k2', k2)
  
  plt.plot(x, y, 'k.:', color='black', label='euler aprimorado', linewidth=1)
  if (calcAnalytics):
    table.add_column('solução exata', analytics)
    table.add_column('erro', error)
    plt.plot(x, analytics, color='blue', label='solução exata', linewidth=2)
  
  table.align = 'r'
  print(table)
  plt.ylabel('y')
  plt.xlabel('x')
  plt.grid()
  plt.legend()
  plt.show()

euler(0, 1, 0.1, 1)
