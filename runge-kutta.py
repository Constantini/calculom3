import math
import matplotlib.pyplot as plt
from prettytable import PrettyTable

fixed = 4

# não modifique isso
xiPlusOne = lambda x, h: round(x + h, fixed)
yiPlusOne = lambda y, h, k1, k2, k3, k4: round(y + h/6 * (k1 + 2 * k2 + 2 * k3 + k4), fixed)

edo = lambda x , y: round(1 - x + 4 * y, fixed)

calcAnalytics = True
analyticsFunction = lambda x, y: round(1/4 * x - 3/16 + 19/16 * math.exp(4 * x), fixed)


def rungeKutta(x0, stop, h, y0):
  '''
  Parameters
  ----------
  x0 : int
    limite inferior do intervalo
  stop : int
    valor de xi+1, criterio de parada
  h : int
    tamanho do passo
  y0: int
    solucao inicial     
  '''
  x = [x0]
  y = [y0]

  # muda
  print(f'1 - {x[len(x) -1]} + 4 * {y[len(y) -1]} k1\n')
  k1 = [edo(x[0], y[0])]

  # muda
  print(f'1 - {(x[len(x) -1] + 0.5 * h)} + 4 * {(y[len(y) -1] + 0.5 * h * k1[len(k1) -1])} k2\n')
  k2 = [edo((x[0] + 0.5 * h), (y[0] + 0.5 * h * k1[0]))]

  # muda
  print(f'1 - {(x[len(x) -1] + 0.5 * h)} + 4 * {(y[len(y) -1] + 0.5 * h * k2[len(k2) -1])} k3\n')
  k3 = [edo((x[0] + 0.5 * h), (y[0] + 0.5 * h * k2[0]))]

  # muda
  print(f'1 - {(x[len(x) -1] + h)} + 4 * {(y[len(y) -1] + h * k3[len(k3) -1])} k4\n')
  k4 = [edo((x[0] + h), (y[0] + h * k3[0]))]

  analytics = []
  error = []
  if (calcAnalytics):
    # muda
    print(f'1/4 * {x[len(x) -1]} - 3/16 + 19/16 * {math.exp(4 * x[len(x) -1])} Analytics\n')
    analytics.append(analyticsFunction(x[0], y[0]))

    print(f'|{analytics[len(analytics) -1]} - {y[len(y) -1]}| erro \n')
    error.append(abs(analytics[0] - y[0]))

  while (x[len(x) -1] < stop):
    print(f'{y[len(y) -1]} + {h/6} * ({k1[len(k1) -1]} + 2 * {k2[len(k2) -1]} + 2 * {k3[len(k3) -1]} + {k4[len(k4) -1]}) y\n')
    y.append(yiPlusOne(y[len(y) -1], h, k1[len(k1) -1], k2[len(k2) -1], k3[len(k3) -1], k4[len(k4) -1]))

    print(f'{x[len(x) -1]} + {h} x\n')
    x.append(xiPlusOne(x[len(x) -1], h))

    # muda
    print(f'1 - {x[len(x) -1]} + 4 * {y[len(y) -1]} k1\n')
    k1.append(edo(x[len(x) -1], y[len(y) -1]))

    # muda
    print(f'1 - {(x[len(x) -1] + 0.5 * h)} + 4 * {(y[len(y) -1] + 0.5 * h * k1[len(k1) -1])} k2\n')
    k2.append(edo((x[len(x) -1] + 0.5 * h), (y[len(y) -1] + 0.5 * h * k1[len(k1) -1])))

    # muda
    print(f'1 - {(x[len(x) -1] + 0.5 * h)} + 4 * {(y[len(y) -1] + 0.5 * h * k2[len(k2) -1])} k3\n')
    k3.append(edo((x[len(x) -1] + 0.5 * h), (y[len(y) -1] + 0.5 * h * k2[len(k2) -1])))

    # muda
    print(f'1 - {(x[len(x) -1] + h)} + 4 * {(y[len(y) -1] + h * k3[len(k3) -1])} k4\n')
    k4.append(edo((x[len(x) -1] + h), (y[len(y) -1] + h * k3[len(k3) -1])))
    
    if (calcAnalytics):
      # muda
      print(f'1/4 * {x[len(x) -1]} - 3/16 + 19/16 * {math.exp(4 * x[len(x) -1])} Analytics\n')
      analytics.append(analyticsFunction(x[len(x) -1], y[len(y) -1]))
      
      print(f'|{analytics[len(analytics) -1]} - {y[len(y) -1]}| erro \n')
      error.append(round(abs(analytics[len(analytics) -1] - y[len(y) -1]), fixed))
  
  table = PrettyTable()
  table.add_column('xi', x)
  table.add_column('yi', y)
  table.add_column('k1', k1)
  table.add_column('k2', k2)
  table.add_column('k3', k3)
  table.add_column('k4', k4)

 
  plt.plot(x, y, 'k.:', color='black', label='range kutta', linewidth=1)
  if (calcAnalytics):
    table.add_column('solução exata', analytics)
    table.add_column('erro', error)
    plt.plot(x, analytics, color='blue', label='solução exata', linewidth=2)
  
  table.align = 'r'
  print(table)
  plt.ylabel('y')
  plt.xlabel('x')
  plt.grid()
  plt.legend()
  plt.show()

rungeKutta(0, 1, 0.1, 1)
