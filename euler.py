import math
import matplotlib.pyplot as plt
from prettytable import PrettyTable

fixed = 4

# não modifique isso
xiPlusOne = lambda x, h: round(x + h, fixed)
yiPlusOne = lambda y, f, h: round(y + f * h, fixed)

edo = lambda x , y: round(1 - x + 4 * y, fixed)

calcAnalytics = True
analyticsFunction = lambda x, y: round(1/4 * x - 3/16 + 19/16 * math.exp(4 * x), fixed)


def euler(x0, stop, h, y0):
  '''
  Parameters
  ----------
  x0 : int
    limite inferior do intervalo
  stop : int
    valor de xi+1, criterio de parada
  h : int
    tamanho do passo
  y0: int
    solucao inicial     
  '''
  x = [x0]
  y = [y0]

  # muda
  print(f'1 - {x[len(x) -1]} + 4 * {y[len(y) -1]} f\n')
  f = [edo(x[0], y[0])]

  analytics = []
  error = []
  if (calcAnalytics):
    # muda
    print(f'1/4 * {x[len(x) -1]} - 3/16 + 19/16 * {math.exp(4 * x[len(x) -1])} Analytics\n')
    analytics.append(analyticsFunction(x[0], y[0]))

    print(f'|{analytics[len(analytics) -1]} - {y[len(y) -1]}| erro \n')
    error.append(abs(analytics[0] - y[0]))

  while (x[len(x) -1] < stop):
    print(f'{y[len(y) -1]} + {edo(x[len(x) -1], y[len(y) -1])} * {h} y\n')
    y.append(yiPlusOne(y[len(y) -1], edo(x[len(x) -1], y[len(y) -1]), h))

    print(f'{x[len(x) -1]} + {h} x\n')
    x.append(xiPlusOne(x[len(x) -1], h))
    
    # muda
    print(f'1 - {x[len(x) -1]} + 4 * {y[len(y) -1]} f\n')
    f.append(edo(x[len(x) -1], y[len(y) -1]))

    if (calcAnalytics):
      # muda
      print(f'1/4 * {x[len(x) -1]} - 3/16 + 19/16 * {math.exp(4 * x[len(x) -1])} Analytics\n')
      analytics.append(analyticsFunction(x[len(x) -1], y[len(y) -1]))

      print(f'|{analytics[len(analytics) -1]} - {y[len(y) -1]}| erro \n')
      error.append(round(abs(analytics[len(analytics) -1] - y[len(y) -1]), fixed))

  table = PrettyTable()
  table.add_column('xi', x)
  table.add_column('yi', y)
  table.add_column("f'(xi, yi)", f)

  plt.plot(x, y, 'k.:', color='black', label='euler', linewidth=1)

  if (calcAnalytics):
    table.add_column('solução exata', analytics)
    table.add_column('erro', error)
    plt.plot(x, analytics, color='blue', label='solução exata', linewidth=2)

  table.align = 'r'
  print(table)

  plt.ylabel('y')
  plt.xlabel('x')
  plt.grid()
  plt.legend()
  plt.show()

euler(0, 1, 0.1, 1)
